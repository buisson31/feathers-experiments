const knex = require('knex')
const config = require('../knexfile.js')

main()

async function main() {
   try {
      let db = knex(config.development)

      let usersTableExists = await db.schema.hasTable('users')
      if (usersTableExists) {
         console.log('table "users" already exists')
      } else {
         await db.schema.createTable('users', table => {
            table.increments('id')
            table.string('email').unique().notNull()
            table.string('password')
            table.timestamp('created_at').defaultTo(db.fn.now()) // 2020-12-02T08:52:00Z
            table.boolean('is_active').defaultTo(false)
            table.enum('role', ['user', 'admin']).notNull()
            table.text('fullname').notNull()
         })
         console.log('table "users" created')
      }
      let picturesTableExists = await db.schema.hasTable('pictures')
      if (picturesTableExists) {
         console.log('table "pictures" already exists')
      } else {
         await db.schema.createTable('pictures', table => {
            table.increments('id')
            table.integer('user_id').references('id').inTable('users').notNull().onDelete('cascade')
            table.timestamp('created_at').defaultTo(db.fn.now())
            table.string('path').notNull()
         })
         console.log('table "pictures" created')
      }
      process.exit(0)
   } catch(err) {
      console.log(err.toString())
   }
}