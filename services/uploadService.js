const fs = require('fs')
const mkdirp = require ('mkdirp')
const path = require ('path')

module.exports = function(app) {

    class UploadService {
        async create(data) {
            try {
                const uintArray = new Uint8Array(data.arrayBuffer)
                mkdirp.sync(path.dirname(data.filepath))
                await fs.promises.writeFile(data.filepath, uintArray)
                return 'OK'
            } catch (error) {
                console.log(error.toString())
            }

        }
    }

    app.use("/upload-service", new UploadService())

}