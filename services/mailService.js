
const mail = require('../mail')

module.exports = function(app) {

    class MailService {
        async create(data) {
            await mail.sendMail(data)
            return 'OK'
        }
    }

    app.use("/mail-service", new MailService())

}