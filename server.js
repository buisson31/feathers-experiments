const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const service = require('feathers-knex')
const knex = require('knex')
const socketio = require('@feathersjs/socketio')

const mailService = require("./services/mailService")
const uploadService = require("./services/uploadService")


let config = require('./knexfile.js')
let database = knex(config.development)

// Create a feathers instance.
const app = express(feathers())

app.use(express.json())
app.use(express.raw({ limit: '100mb'}))

// Transport: HTTP & websocket
app.configure(express.rest())
app.configure(socketio())

//HTML static page service
app.use(express.static('./static'))
// Custom services
app.configure(mailService)
app.configure(uploadService)

// Feathers REST services
let userService = service({Model: database, name: 'users'})
let pictureService = service({Model: database, name: 'pictures'})

app.use('/api/users', userService)
app.use('/api/pictures', pictureService)


// Start the server.
const port = process.env.PORT || 3000

app.listen(port, async () => {
  console.log(`Server listening on port ${port}`)
})