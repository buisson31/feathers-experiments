const knex = require('knex')
const config = require('./knexfile.js')
const service = require('feathers-knex')

main()

async function main() {
   try {
      let database = knex(config.development)
      let userService = service({Model: database, name: 'users'})

      let activeUsers = await userService.find({
         query: {
            is_active: true,
         }
      })
      console.log('activeUsers', activeUsers)

   } catch(err) {
      console.log(err.toString())
   } finally {
      process.exit(0)
   }
}
