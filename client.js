const io = require('socket.io-client')
const feathers = require('@feathersjs/feathers')
const socketio = require('@feathersjs/socketio-client')

const socket = io('http://localhost:3000')
const fs = require ('fs')

const app = feathers()

app.configure(socketio(socket))


async function main() {
   //let user2 = await app.service('/api/users').get(2)
   //console.log('user2', user2)
   let arrayBuffer = await fs.promises.readFile('/home/mariam/iStock-613015246.webp')

   let x = await app.service('/upload-service').create({
      filepath: 'upload/images/image.webp',
      arrayBuffer,
   })

   
   // console.log('x', x)
   process.exit(0)
}

main()
